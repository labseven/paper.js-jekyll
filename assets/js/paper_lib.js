function random(min, max){
    return min + (Math.random() * (max-min));
}

function gaussianRand() {
    var rand = 0;
    for (var i = 0; i < 6; i += 1) {
      rand += Math.random();
    }
    return rand / 6;
}

function randomChance(prob){
    return Math.random() < prob;
}

function randomPick(array){
    return Math.floor(Math.random() * array.length);
}

function fiftyfifty(){ return randomChance(.5); }

function gRandom(min, max){
    return min + (gaussianRand() * (max-min));
}

function randInt(min, max) {
    return Math.floor(random(min, max))
}

function x_s(n) {
    return n * (view.size.width / 100);
}

function ls(min, max, i) {
    return (Math.pow(i,2) * (max - min)) + min;
}

function lsInt(min, max, i) {
    return Math.round(ls(min, max, i));
}


function* time_rand(min, max, speed=0.001) {
    const seed = Math.random();
    t = 0
   

    console.log(seed);
    

    while(true){
        t += speed        
        yield min + (Math.abs(noise.perlin2(seed * 200, t)) * (max-min));        
    }
}


function fill_shape(shape, num_circ=-1) {
    if(num_circ === -1){
        num_circ = shape.data.num_circ;
    }

    shapes = [shape]

    for(i=num_circ; i>0; i--){
        new_shape = shape.clone();
        new_shape.scale(ls(.01,1, i/num_circ), shape.center)
        new_shape.bringToFront()
        shapes.push(new_shape)
    }
    return new CompoundPath({
                children: shapes,
                strokeColor: "black",
                fillColor: "white",
                strokeWidth: 1.5
            })
}
