(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global = global || self, global.CanvasHpgl = factory());
}(this, function () { 'use strict';

  /**
   * Calculate a point transformed with an affine matrix
   * @param matrix Affine matrix
   * @param point Point
   * @returns {{x: number, y: number} | Array} Point
   */
  function applyToPoint(matrix, point) {
    return Array.isArray(point) ? [matrix.a * point[0] + matrix.c * point[1] + matrix.e, matrix.b * point[0] + matrix.d * point[1] + matrix.f] : {
      x: matrix.a * point.x + matrix.c * point.y + matrix.e,
      y: matrix.b * point.x + matrix.d * point.y + matrix.f
    };
  }

  /**
   * Extract an affine matrix from an object that contains a,b,c,d,e,f keys
   * Each value could be a float or a string that contains a float
   * @param object
   * @return {{a: *, b: *, c: *, e: *, d: *, f: *}}}
   */
  function fromObject(object) {
    return {
      a: parseFloat(object.a),
      b: parseFloat(object.b),
      c: parseFloat(object.c),
      d: parseFloat(object.d),
      e: parseFloat(object.e),
      f: parseFloat(object.f)
    };
  }

  /**
   * @ignore
   * @type {RegExp}
   */

  /**
   * Identity matrix
   * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
   */
  function identity() {
    return {
      a: 1,
      c: 0,
      e: 0,
      b: 0,
      d: 1,
      f: 0
    };
  }

  /**
   * Calculate a matrix that is the inverse of the provided matrix
   * @param matrix Affine matrix
   * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
   */

  function isUndefined(val) {
    return typeof val === 'undefined';
  }

  /**
   * Calculate a translate matrix
   * @param tx Translation on axis x
   * @param [ty = 0] Translation on axis y
   * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
   */
  function translate(tx) {
    var ty = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

    return {
      a: 1,
      c: 0,
      e: tx,
      b: 0,
      d: 1,
      f: ty
    };
  }

  function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

  function _toArray(arr) { return Array.isArray(arr) ? arr : Array.from(arr); }

  /**
   * Merge multiple matrices into one
   * @param matrices {...object} list of matrices
   * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
   */
  function transform() {
    var arguments$1 = arguments;

    for (var _len = arguments.length, matrices = Array(_len), _key = 0; _key < _len; _key++) {
      matrices[_key] = arguments$1[_key];
    }

    matrices = Array.isArray(matrices[0]) ? matrices[0] : matrices;

    var multiply = function multiply(m1, m2) {
      return {
        a: m1.a * m2.a + m1.c * m2.b,
        c: m1.a * m2.c + m1.c * m2.d,
        e: m1.a * m2.e + m1.c * m2.f + m1.e,
        b: m1.b * m2.a + m1.d * m2.b,
        d: m1.b * m2.c + m1.d * m2.d,
        f: m1.b * m2.e + m1.d * m2.f + m1.f
      };
    };

    switch (matrices.length) {
      case 0:
        throw new Error('no matrices provided');

      case 1:
        return matrices[0];

      case 2:
        return multiply(matrices[0], matrices[1]);

      default:
        var _matrices = matrices,
            _matrices2 = _toArray(_matrices),
            m1 = _matrices2[0],
            m2 = _matrices2[1],
            rest = _matrices2.slice(2);

        var m = multiply(m1, m2);
        return transform.apply(undefined, [m].concat(_toConsumableArray(rest)));
    }
  }

  /**
   * Merge multiple matrices into one (alias of `transform`)
   * @param matrices {...object} list of matrices
   * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
   */
  function compose() {
    return transform.apply(undefined, arguments);
  }

  var cos = Math.cos,
      sin = Math.sin,
      PI = Math.PI;
  /**
   * Calculate a rotation matrix
   * @param angle Angle in radians
   * @param [cx] If (cx,cy) are supplied the rotate is about this point
   * @param [cy] If (cx,cy) are supplied the rotate is about this point
   * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix *
   */

  function rotate(angle, cx, cy) {
    var cosAngle = cos(angle);
    var sinAngle = sin(angle);
    var rotationMatrix = {
      a: cosAngle,
      c: -sinAngle,
      e: 0,
      b: sinAngle,
      d: cosAngle,
      f: 0
    };
    if (isUndefined(cx) || isUndefined(cy)) {
      return rotationMatrix;
    }

    return transform([translate(cx, cy), rotationMatrix, translate(-cx, -cy)]);
  }

  /**
   * Calculate a scaling matrix
   * @param sx Scaling on axis x
   * @param [sy = sx] Scaling on axis y (default sx)
   * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
   */
  function scale(sx) {
    var sy = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;

    if (isUndefined(sy)) { sy = sx; }
    return {
      a: sx,
      c: 0,
      e: 0,
      b: 0,
      d: sy,
      f: 0
    };
  }

  /**
   * Calculate a shear matrix
   * @param shx Shear on axis x
   * @param shy Shear on axis y
   * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
   */

  // https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/skew

  /**
   * Serialize the matrix to a string that can be used with CSS or SVG
   * @param matrix Affine matrix
   * @returns {string} String that contains a matrix formatted as matrix(a,b,c,d,e,f)
   */

  var round = Math.round;
  var ln = ";\n";

  var CanvasHpgl = function CanvasHpgl(ref) {
    if ( ref === void 0 ) ref = {};
    var penColors = ref.penColors;

    this.penColors = penColors || {};
    this.beginPath();
    this.resetTransform();

    // store the strokeStyle for reference, this will be a pain when adding save/restore
    this._strokeStyle = "#000000";
  };

  var prototypeAccessors = { strokeStyle: { configurable: true } };

  prototypeAccessors.strokeStyle.set = function (color) {
    var pen = this.penColors[color];
    this._cmd("SP", pen || 1);
    this._strokeStyle = color;
  };

  prototypeAccessors.strokeStyle.get = function () {
    return this._strokeStyle;
  };

  CanvasHpgl.prototype.rotate = function rotate$1 (a, cx, cy) {
    this._matrix = compose(
      this._matrix,
      rotate(a, cx, cy)
    );
  };
  CanvasHpgl.prototype.scale = function scale$1 (sx, sy) {
    this._matrix = compose(
      this._matrix,
      scale(sx, sy)
    );
  };
  CanvasHpgl.prototype.translate = function translate$1 (tx, ty) {
    this._matrix = compose(
      this._matrix,
      translate(tx, ty)
    );
  };
  CanvasHpgl.prototype.transform = function transform$$1 (a, b, c, d, e, f) {
    this._matrix = compose(
      this._matrix,
      fromObject({ a: a, b: b, c: c, d: d, e: e, f: f })
    );
  };
  CanvasHpgl.prototype.resetTransform = function resetTransform () {
    this._transformStack = [];
    this._matrix = identity();
  };
  CanvasHpgl.prototype.save = function save () {
    this._transformStack.push(toString(this._matrix));
  };
  CanvasHpgl.prototype.restore = function restore () {
    if (this._transformStack.length) {
      this._matrix = fromString(this._transformStack.pop());
    }
  };

  CanvasHpgl.prototype.beginPath = function beginPath () {
    this._ = [];
    this._x0 = this._y0 = null; // start of current subpath
    this._x1 = this._y1 = null; // end of current subpath
  };

  CanvasHpgl.prototype.moveTo = function moveTo (x, y) {
      var assign;

    y *= -1;
    ((assign = applyToPoint(this._matrix, { x: x, y: y }), x = assign.x, y = assign.y));
    this._cmd(
      "PU",
      round((this._x0 = this._x1 = +x)),
      round((this._y0 = this._y1 = +y))
    );
  };
  CanvasHpgl.prototype.lineTo = function lineTo (x, y) {
      var assign;

    y *= -1;
    ((assign = applyToPoint(this._matrix, { x: x, y: y }), x = assign.x, y = assign.y));
    this._cmd("PD", round((this._x1 = +x)), round((this._y1 = +y)));
  };
  CanvasHpgl.prototype.toString = function toString () {
    if (!this._.length) { return ""; }
    return this._.map(function (d) { return d.join(" "); }).join(ln) + ln;
  };

  CanvasHpgl.prototype._cmd = function _cmd (cmd) {
      var params = [], len = arguments.length - 1;
      while ( len-- > 0 ) params[ len ] = arguments[ len + 1 ];

    params = params.join(" ");
    if (this._.length) {
      var prevCmd = this._[this._.length - 1];

      // Replace previous move commands with the new one
      // to remove long lists of pointless consecutive moves
      if (cmd === "PU" && prevCmd[0] === "PU") {
        this._.pop();
        this._.push([cmd, params]);
        return;
      }
        
      // Don't move/draw to the same point if we're already there
      if (prevCmd[1] === params) {
        return;
      }
    }
    this._.push([cmd, params]);
  };

  Object.defineProperties( CanvasHpgl.prototype, prototypeAccessors );

  return CanvasHpgl;

}));
//# sourceMappingURL=canvas-hpgl.umd.js.map
