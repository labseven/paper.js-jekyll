(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    (global.CanvasPolyline = factory());
}(this, (function () { 'use strict';

    function clone(point) { //TODO: use gl-vec2 for this
        return [point[0], point[1]]
    }

    function vec2(x, y) {
        return [x, y]
    }

    var _function = function createBezierBuilder(opt) {
        opt = opt||{};

        var RECURSION_LIMIT = typeof opt.recursion === 'number' ? opt.recursion : 8;
        var FLT_EPSILON = typeof opt.epsilon === 'number' ? opt.epsilon : 1.19209290e-7;
        var PATH_DISTANCE_EPSILON = typeof opt.pathEpsilon === 'number' ? opt.pathEpsilon : 1.0;

        var curve_angle_tolerance_epsilon = typeof opt.angleEpsilon === 'number' ? opt.angleEpsilon : 0.01;
        var m_angle_tolerance = opt.angleTolerance || 0;
        var m_cusp_limit = opt.cuspLimit || 0;

        return function bezierCurve(start, c1, c2, end, scale, points) {
            if (!points)
                { points = []; }

            scale = typeof scale === 'number' ? scale : 1.0;
            var distanceTolerance = PATH_DISTANCE_EPSILON / scale;
            distanceTolerance *= distanceTolerance;
            begin(start, c1, c2, end, points, distanceTolerance);
            return points
        }


        ////// Based on:
        ////// https://github.com/pelson/antigrain/blob/master/agg-2.4/src/agg_curves.cpp

        function begin(start, c1, c2, end, points, distanceTolerance) {
            points.push(clone(start));
            var x1 = start[0],
                y1 = start[1],
                x2 = c1[0],
                y2 = c1[1],
                x3 = c2[0],
                y3 = c2[1],
                x4 = end[0],
                y4 = end[1];
            recursive(x1, y1, x2, y2, x3, y3, x4, y4, points, distanceTolerance, 0);
            points.push(clone(end));
        }

        function recursive(x1, y1, x2, y2, x3, y3, x4, y4, points, distanceTolerance, level) {
            if(level > RECURSION_LIMIT) 
                { return }

            var pi = Math.PI;

            // Calculate all the mid-points of the line segments
            //----------------------
            var x12   = (x1 + x2) / 2;
            var y12   = (y1 + y2) / 2;
            var x23   = (x2 + x3) / 2;
            var y23   = (y2 + y3) / 2;
            var x34   = (x3 + x4) / 2;
            var y34   = (y3 + y4) / 2;
            var x123  = (x12 + x23) / 2;
            var y123  = (y12 + y23) / 2;
            var x234  = (x23 + x34) / 2;
            var y234  = (y23 + y34) / 2;
            var x1234 = (x123 + x234) / 2;
            var y1234 = (y123 + y234) / 2;

            if(level > 0) { // Enforce subdivision first time
                // Try to approximate the full cubic curve by a single straight line
                //------------------
                var dx = x4-x1;
                var dy = y4-y1;

                var d2 = Math.abs((x2 - x4) * dy - (y2 - y4) * dx);
                var d3 = Math.abs((x3 - x4) * dy - (y3 - y4) * dx);

                var da1, da2;

                if(d2 > FLT_EPSILON && d3 > FLT_EPSILON) {
                    // Regular care
                    //-----------------
                    if((d2 + d3)*(d2 + d3) <= distanceTolerance * (dx*dx + dy*dy)) {
                        // If the curvature doesn't exceed the distanceTolerance value
                        // we tend to finish subdivisions.
                        //----------------------
                        if(m_angle_tolerance < curve_angle_tolerance_epsilon) {
                            points.push(vec2(x1234, y1234));
                            return
                        }

                        // Angle & Cusp Condition
                        //----------------------
                        var a23 = Math.atan2(y3 - y2, x3 - x2);
                        da1 = Math.abs(a23 - Math.atan2(y2 - y1, x2 - x1));
                        da2 = Math.abs(Math.atan2(y4 - y3, x4 - x3) - a23);
                        if(da1 >= pi) { da1 = 2*pi - da1; }
                        if(da2 >= pi) { da2 = 2*pi - da2; }

                        if(da1 + da2 < m_angle_tolerance) {
                            // Finally we can stop the recursion
                            //----------------------
                            points.push(vec2(x1234, y1234));
                            return
                        }

                        if(m_cusp_limit !== 0.0) {
                            if(da1 > m_cusp_limit) {
                                points.push(vec2(x2, y2));
                                return
                            }

                            if(da2 > m_cusp_limit) {
                                points.push(vec2(x3, y3));
                                return
                            }
                        }
                    }
                }
                else {
                    if(d2 > FLT_EPSILON) {
                        // p1,p3,p4 are collinear, p2 is considerable
                        //----------------------
                        if(d2 * d2 <= distanceTolerance * (dx*dx + dy*dy)) {
                            if(m_angle_tolerance < curve_angle_tolerance_epsilon) {
                                points.push(vec2(x1234, y1234));
                                return
                            }

                            // Angle Condition
                            //----------------------
                            da1 = Math.abs(Math.atan2(y3 - y2, x3 - x2) - Math.atan2(y2 - y1, x2 - x1));
                            if(da1 >= pi) { da1 = 2*pi - da1; }

                            if(da1 < m_angle_tolerance) {
                                points.push(vec2(x2, y2));
                                points.push(vec2(x3, y3));
                                return
                            }

                            if(m_cusp_limit !== 0.0) {
                                if(da1 > m_cusp_limit) {
                                    points.push(vec2(x2, y2));
                                    return
                                }
                            }
                        }
                    }
                    else if(d3 > FLT_EPSILON) {
                        // p1,p2,p4 are collinear, p3 is considerable
                        //----------------------
                        if(d3 * d3 <= distanceTolerance * (dx*dx + dy*dy)) {
                            if(m_angle_tolerance < curve_angle_tolerance_epsilon) {
                                points.push(vec2(x1234, y1234));
                                return
                            }

                            // Angle Condition
                            //----------------------
                            da1 = Math.abs(Math.atan2(y4 - y3, x4 - x3) - Math.atan2(y3 - y2, x3 - x2));
                            if(da1 >= pi) { da1 = 2*pi - da1; }

                            if(da1 < m_angle_tolerance) {
                                points.push(vec2(x2, y2));
                                points.push(vec2(x3, y3));
                                return
                            }

                            if(m_cusp_limit !== 0.0) {
                                if(da1 > m_cusp_limit)
                                {
                                    points.push(vec2(x3, y3));
                                    return
                                }
                            }
                        }
                    }
                    else {
                        // Collinear case
                        //-----------------
                        dx = x1234 - (x1 + x4) / 2;
                        dy = y1234 - (y1 + y4) / 2;
                        if(dx*dx + dy*dy <= distanceTolerance) {
                            points.push(vec2(x1234, y1234));
                            return
                        }
                    }
                }
            }

            // Continue subdivision
            //----------------------
            recursive(x1, y1, x12, y12, x123, y123, x1234, y1234, points, distanceTolerance, level + 1); 
            recursive(x1234, y1234, x234, y234, x34, y34, x4, y4, points, distanceTolerance, level + 1); 
        }
    };

    var adaptiveBezierCurve = _function();

    function clone$1(point) { //TODO: use gl-vec2 for this
        return [point[0], point[1]]
    }

    function vec2$1(x, y) {
        return [x, y]
    }

    var _function$1 = function createQuadraticBuilder(opt) {
        opt = opt||{};

        var RECURSION_LIMIT = typeof opt.recursion === 'number' ? opt.recursion : 8;
        var FLT_EPSILON = typeof opt.epsilon === 'number' ? opt.epsilon : 1.19209290e-7;
        var PATH_DISTANCE_EPSILON = typeof opt.pathEpsilon === 'number' ? opt.pathEpsilon : 1.0;

        var curve_angle_tolerance_epsilon = typeof opt.angleEpsilon === 'number' ? opt.angleEpsilon : 0.01;
        var m_angle_tolerance = opt.angleTolerance || 0;

        return function quadraticCurve(start, c1, end, scale, points) {
            if (!points)
                { points = []; }

            scale = typeof scale === 'number' ? scale : 1.0;
            var distanceTolerance = PATH_DISTANCE_EPSILON / scale;
            distanceTolerance *= distanceTolerance;
            begin(start, c1, end, points, distanceTolerance);
            return points
        }

        ////// Based on:
        ////// https://github.com/pelson/antigrain/blob/master/agg-2.4/src/agg_curves.cpp

        function begin(start, c1, end, points, distanceTolerance) {
            points.push(clone$1(start));
            var x1 = start[0],
                y1 = start[1],
                x2 = c1[0],
                y2 = c1[1],
                x3 = end[0],
                y3 = end[1];
            recursive(x1, y1, x2, y2, x3, y3, points, distanceTolerance, 0);
            points.push(clone$1(end));
        }



        function recursive(x1, y1, x2, y2, x3, y3, points, distanceTolerance, level) {
            if(level > RECURSION_LIMIT) 
                { return }

            var pi = Math.PI;

            // Calculate all the mid-points of the line segments
            //----------------------
            var x12   = (x1 + x2) / 2;                
            var y12   = (y1 + y2) / 2;
            var x23   = (x2 + x3) / 2;
            var y23   = (y2 + y3) / 2;
            var x123  = (x12 + x23) / 2;
            var y123  = (y12 + y23) / 2;

            var dx = x3-x1;
            var dy = y3-y1;
            var d = Math.abs(((x2 - x3) * dy - (y2 - y3) * dx));

            if(d > FLT_EPSILON)
            { 
                // Regular care
                //-----------------
                if(d * d <= distanceTolerance * (dx*dx + dy*dy))
                {
                    // If the curvature doesn't exceed the distance_tolerance value
                    // we tend to finish subdivisions.
                    //----------------------
                    if(m_angle_tolerance < curve_angle_tolerance_epsilon)
                    {
                        points.push(vec2$1(x123, y123));
                        return
                    }

                    // Angle & Cusp Condition
                    //----------------------
                    var da = Math.abs(Math.atan2(y3 - y2, x3 - x2) - Math.atan2(y2 - y1, x2 - x1));
                    if(da >= pi) { da = 2*pi - da; }

                    if(da < m_angle_tolerance)
                    {
                        // Finally we can stop the recursion
                        //----------------------
                        points.push(vec2$1(x123, y123));
                        return                 
                    }
                }
            }
            else
            {
                // Collinear case
                //-----------------
                dx = x123 - (x1 + x3) / 2;
                dy = y123 - (y1 + y3) / 2;
                if(dx*dx + dy*dy <= distanceTolerance)
                {
                    points.push(vec2$1(x123, y123));
                    return
                }
            }

            // Continue subdivision
            //----------------------
            recursive(x1, y1, x12, y12, x123, y123, points, distanceTolerance, level + 1); 
            recursive(x123, y123, x23, y23, x3, y3, points, distanceTolerance, level + 1); 
        }
    };

    var adaptiveQuadraticCurve = _function$1();

    /**
     * Calculate a point transformed with an affine matrix
     * @param matrix Affine matrix
     * @param point Point
     * @returns {{x: number, y: number} | Array} Point
     */
    function applyToPoint(matrix, point) {
      return Array.isArray(point) ? [matrix.a * point[0] + matrix.c * point[1] + matrix.e, matrix.b * point[0] + matrix.d * point[1] + matrix.f] : {
        x: matrix.a * point.x + matrix.c * point.y + matrix.e,
        y: matrix.b * point.x + matrix.d * point.y + matrix.f
      };
    }

    /**
     * Extract an affine matrix from an object that contains a,b,c,d,e,f keys
     * Each value could be a float or a string that contains a float
     * @param object
     * @return {{a: *, b: *, c: *, e: *, d: *, f: *}}}
     */
    function fromObject(object) {
      return {
        a: parseFloat(object.a),
        b: parseFloat(object.b),
        c: parseFloat(object.c),
        d: parseFloat(object.d),
        e: parseFloat(object.e),
        f: parseFloat(object.f)
      };
    }

    /**
     * @ignore
     * @type {RegExp}
     */
    var matrixRegex = /^matrix\(\s*([0-9_+-.e]+)\s*,\s*([0-9_+-.e]+)\s*,\s*([0-9_+-.e]+)\s*,\s*([0-9_+-.e]+)\s*,\s*([0-9_+-.e]+)\s*,\s*([0-9_+-.e]+)\s*\)$/i;

    /**
     * Parse a string matrix formatted as matrix(a,b,c,d,e,f)
     * @param string String with a matrix
     * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
     */
    function fromString(string) {
      var parsed = string.match(matrixRegex);
      if (parsed === null || parsed.length < 7) { throw new Error("'" + string + "' is not a matrix"); }
      return {
        a: parseFloat(parsed[1]),
        b: parseFloat(parsed[2]),
        c: parseFloat(parsed[3]),
        d: parseFloat(parsed[4]),
        e: parseFloat(parsed[5]),
        f: parseFloat(parsed[6])
      };
    }

    /**
     * Identity matrix
     * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
     */
    function identity() {
      return {
        a: 1,
        c: 0,
        e: 0,
        b: 0,
        d: 1,
        f: 0
      };
    }

    /**
     * Calculate a matrix that is the inverse of the provided matrix
     * @param matrix Affine matrix
     * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
     */
    function inverse(matrix) {
      // http://www.wolframalpha.com/input/?i=Inverse+%5B%7B%7Ba,c,e%7D,%7Bb,d,f%7D,%7B0,0,1%7D%7D%5D

      var a = matrix.a,
          b = matrix.b,
          c = matrix.c,
          d = matrix.d,
          e = matrix.e,
          f = matrix.f;


      var denom = a * d - b * c;

      return {
        a: d / denom,
        b: b / -denom,
        c: c / -denom,
        d: a / denom,
        e: (d * e - c * f) / -denom,
        f: (b * e - a * f) / denom
      };
    }

    function isUndefined(val) {
      return typeof val === 'undefined';
    }

    /**
     * Calculate a translate matrix
     * @param tx Translation on axis x
     * @param [ty = 0] Translation on axis y
     * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
     */
    function translate(tx) {
      var ty = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

      return {
        a: 1,
        c: 0,
        e: tx,
        b: 0,
        d: 1,
        f: ty
      };
    }

    function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

    function _toArray(arr) { return Array.isArray(arr) ? arr : Array.from(arr); }

    /**
     * Merge multiple matrices into one
     * @param matrices {...object} list of matrices
     * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
     */
    function transform() {
      var arguments$1 = arguments;

      for (var _len = arguments.length, matrices = Array(_len), _key = 0; _key < _len; _key++) {
        matrices[_key] = arguments$1[_key];
      }

      matrices = Array.isArray(matrices[0]) ? matrices[0] : matrices;

      var multiply = function multiply(m1, m2) {
        return {
          a: m1.a * m2.a + m1.c * m2.b,
          c: m1.a * m2.c + m1.c * m2.d,
          e: m1.a * m2.e + m1.c * m2.f + m1.e,
          b: m1.b * m2.a + m1.d * m2.b,
          d: m1.b * m2.c + m1.d * m2.d,
          f: m1.b * m2.e + m1.d * m2.f + m1.f
        };
      };

      switch (matrices.length) {
        case 0:
          throw new Error('no matrices provided');

        case 1:
          return matrices[0];

        case 2:
          return multiply(matrices[0], matrices[1]);

        default:
          var _matrices = matrices,
              _matrices2 = _toArray(_matrices),
              m1 = _matrices2[0],
              m2 = _matrices2[1],
              rest = _matrices2.slice(2);

          var m = multiply(m1, m2);
          return transform.apply(undefined, [m].concat(_toConsumableArray(rest)));
      }
    }

    /**
     * Merge multiple matrices into one (alias of `transform`)
     * @param matrices {...object} list of matrices
     * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
     */
    function compose() {
      return transform.apply(undefined, arguments);
    }

    var cos = Math.cos,
        sin = Math.sin,
        PI = Math.PI;
    /**
     * Calculate a rotation matrix
     * @param angle Angle in radians
     * @param [cx] If (cx,cy) are supplied the rotate is about this point
     * @param [cy] If (cx,cy) are supplied the rotate is about this point
     * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix *
     */

    function rotate(angle, cx, cy) {
      var cosAngle = cos(angle);
      var sinAngle = sin(angle);
      var rotationMatrix = {
        a: cosAngle,
        c: -sinAngle,
        e: 0,
        b: sinAngle,
        d: cosAngle,
        f: 0
      };
      if (isUndefined(cx) || isUndefined(cy)) {
        return rotationMatrix;
      }

      return transform([translate(cx, cy), rotationMatrix, translate(-cx, -cy)]);
    }

    /**
     * Calculate a scaling matrix
     * @param sx Scaling on axis x
     * @param [sy = sx] Scaling on axis y (default sx)
     * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
     */
    function scale(sx) {
      var sy = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;

      if (isUndefined(sy)) { sy = sx; }
      return {
        a: sx,
        c: 0,
        e: 0,
        b: 0,
        d: sy,
        f: 0
      };
    }

    /**
     * Calculate a shear matrix
     * @param shx Shear on axis x
     * @param shy Shear on axis y
     * @returns {{a: number, b: number, c: number, e: number, d: number, f: number}} Affine matrix
     */

    // https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/skew

    /**
     * Serialize the matrix to a string that can be used with CSS or SVG
     * @param matrix Affine matrix
     * @returns {string} String that contains a matrix formatted as matrix(a,b,c,d,e,f)
     */

    /**
     * Serialize the matrix to a string that can be used with CSS or SVG
     * @param matrix Affine matrix
     * @returns {string} String that contains a matrix formatted as matrix(a,b,c,d,e,f)
     */
    function toString(matrix) {
      return "matrix(" + matrix.a + "," + matrix.b + "," + matrix.c + "," + matrix.d + "," + matrix.e + "," + matrix.f + ")";
    }

    var pi = Math.PI,
      tau = 2 * pi,
      epsilon = 1e-6,
      defaultCurveScale = 2,
      defaultArcScale = 2;

    var CanvasPolyLine = function CanvasPolyLine(ctx) {
      this.ctx = Array.isArray(ctx) ? ctx : [ctx];
      this._strokeStyle = "#000000";
      this._arcScale = defaultArcScale;
      this._curveScale = defaultCurveScale;
      this._stack = [];
      this.beginPath();
      this.resetTransform();
    };

    var prototypeAccessors = { strokeStyle: { configurable: true },arcScale: { configurable: true },curveScale: { configurable: true } };

    prototypeAccessors.strokeStyle.get = function () {
      return this._strokeStyle;
    };
    prototypeAccessors.strokeStyle.set = function (_) {
      this._strokeStyle = _;
      this.ctx.forEach(function (c) { return (c.strokeStyle = _); });
    };
    prototypeAccessors.arcScale.set = function (_) {
      this._arcScale = _;
    };
    prototypeAccessors.curveScale.set = function (_) {
      this._curveScale = _;
    };

    CanvasPolyLine.prototype.stroke = function stroke () {
      this.ctx.forEach(function (c) { return c.stroke(); });
    };
    CanvasPolyLine.prototype.strokeRect = function strokeRect (x, y, width, height) {
      this.rect(x, y, width, height);
      this.ctx.forEach(function (c) { return c.stroke(); });
    };
    CanvasPolyLine.prototype.rotate = function rotate$1 (a, cx, cy) {
      this._matrix = compose(
        this._matrix,
        rotate(a, cx, cy)
      );
    };
    CanvasPolyLine.prototype.scale = function scale$1 (sx, sy) {
      this._matrix = compose(
        this._matrix,
        scale(sx, sy)
      );
    };
    CanvasPolyLine.prototype.translate = function translate$1 (tx, ty) {
      this._matrix = compose(
        this._matrix,
        translate(tx, ty)
      );
    };
    CanvasPolyLine.prototype.transform = function transform$$1 (a, b, c, d, e, f) {
      this._matrix = compose(
        this._matrix,
        fromObject({ a: a, b: b, c: c, d: d, e: e, f: f })
      );
    };
    CanvasPolyLine.prototype.resetTransform = function resetTransform () {
      this._matrix = identity();
    };
    CanvasPolyLine.prototype.save = function save () {
      this._stack.push({
        _matrix: toString(this._matrix),
        _strokeStyle: this._strokeStyle
      });
    };
    CanvasPolyLine.prototype.restore = function restore () {
      if (this._stack.length) {
        var ref = this._stack.pop();
          var _matrix = ref._matrix;
          var _strokeStyle = ref._strokeStyle;
        this._matrix = fromString(_matrix);
        this._strokeStyle = _strokeStyle;
      }
    };

    CanvasPolyLine.prototype.beginPath = function beginPath () {
      this._x0 = this._y0 = null; // start of current subpath
      this._x1 = this._y1 = null; // end of current subpath
    };
    CanvasPolyLine.prototype.moveTo = function moveTo (x, y) {
        var assign;

      (assign = applyToPoint(this._matrix, [x, y]), x = assign[0], y = assign[1]);
      this._x0 = this._x1 = +x;
      this._y0 = this._y1 = +y;
      this.ctx.forEach(function (c) { return c.moveTo(x, y); });
    };
    CanvasPolyLine.prototype.lineTo = function lineTo (x, y) {
        var assign;

      (assign = applyToPoint(this._matrix, [x, y]), x = assign[0], y = assign[1]);
      this._x1 = +x;
      this._y1 = +y;
      this.ctx.forEach(function (c) { return c.lineTo(x, y); });
    };
    CanvasPolyLine.prototype.closePath = function closePath () {
      if (this._x1 !== null) {
        var start = applyToPoint(inverse(this._matrix), [this._x0, this._y0]);
        this.lineTo(start[0], start[1]);
      }
    };
    CanvasPolyLine.prototype.quadraticCurveTo = function quadraticCurveTo (x1, y1, x, y) {
        var this$1 = this;

      var start = applyToPoint(inverse(this._matrix), [this._x1, this._y1]);
      var end = applyToPoint(this._matrix, [x, y]);
      var points = adaptiveQuadraticCurve(start, [x1, y1], [+x, +y], +this._curveScale);
      this._x0 = this._x1 = end[0];
      this._y0 = this._y1 = end[1];
      points.forEach(function (p) {
        this$1.lineTo(p[0], p[1]);
      });
    };
    CanvasPolyLine.prototype.bezierCurveTo = function bezierCurveTo (x1, y1, x2, y2, x, y) {
        var this$1 = this;

      var startIsEnd = arraysEqual([this._x0, this._y0], [this._x1, this._y1]);
      var start = startIsEnd
        ? applyToPoint(inverse(this._matrix), [this._x1, this._y1])
        : [this._x0, this._y0];
      var end = applyToPoint(this._matrix, [x, y]);

      var points = adaptiveBezierCurve(
        start,
        [x1, y1],
        [x2, y2],
        [(this._x0 = this._x1 = +x), (this._y0 = this._y1 = +y)],
        +this._curveScale
      );
      this._x0 = this._x1 = end[0];
      this._y0 = this._y1 = end[1];
      points.forEach(function (p) {
        this$1.lineTo(p[0], p[1]);
      });
    };
    CanvasPolyLine.prototype.arc = function arc (x, y, r, a0, a1, ccw) {
      this.ellipse(x, y, r, r, 0, a0, a1, ccw);
    };
    CanvasPolyLine.prototype.arcTo = function arcTo (x1, y1, x2, y2, r) {
        var assign, assign$1;

      (x1 = +x1), (y1 = +y1), (x2 = +x2), (y2 = +y2), (r = +r);

      var ref = applyToPoint(inverse(this._matrix), [this._x1, this._y1]);
        var x0 = ref[0];
        var y0 = ref[1];

      var x21 = x2 - x1,
        y21 = y2 - y1,
        x01 = x0 - x1,
        y01 = y0 - y1,
        l01_2 = x01 * x01 + y01 * y01;

      // Is the radius negative? Error.
      if (r < 0) { throw new Error("negative radius: " + r); }

      // Is this path empty? Move to (x1,y1).
      if (this._x1 === null) {
        this.moveTo(x1, y1);
      }

      // Or, is (x1,y1) coincident with (x0,y0)? Do nothing.
      else if (!(l01_2 > epsilon));
      else if (!(Math.abs(y01 * x21 - y21 * x01) > epsilon) || !r) {
        // Or, are (x0,y0), (x1,y1) and (x2,y2) collinear?
        // Equivalently, is (x1,y1) coincident with (x2,y2)?
        // Or, is the radius zero? Line to (x1,y1).
        // this._ += "L" + (this._x1 = x1) + "," + (this._y1 = y1);
        this.lineTo(x1, y1);
        (assign = applyToPoint(inverse(this._matrix), [this._x1, this._y1]), x0 = assign[0], y0 = assign[1]);
      }

      // Otherwise, draw an arc!
      else {
        var x20 = x2 - x0,
          y20 = y2 - y0,
          l21_2 = x21 * x21 + y21 * y21,
          l20_2 = x20 * x20 + y20 * y20,
          l21 = Math.sqrt(l21_2),
          l01 = Math.sqrt(l01_2),
          l =
            r *
            Math.tan(
              (pi - Math.acos((l21_2 + l01_2 - l20_2) / (2 * l21 * l01))) / 2
            ),
          t01 = l / l01,
          t21 = l / l21;

        // If the start tangent is not coincident with (x0,y0), line to.
        if (Math.abs(t01 - 1) > epsilon) {
          // this._ += "L" + (x1 + t01 * x01) + "," + (y1 + t01 * y01);
          this.lineTo(x1 + t01 * x01, y1 + t01 * y01);
          (assign$1 = applyToPoint(inverse(this._matrix), [this._x1, this._y1]), x0 = assign$1[0], y0 = assign$1[1]);
        }

        var x3 = x1 + t21 * x21;
        var y3 = y1 + t21 * y21;

        var h = Math.hypot(x3 - x0, y3 - y0);
        var a = Math.asin(h / 2 / r) * 2;

        var x4 = (x0 + x3) / 2;
        var y4 = (y0 + y3) / 2;

        var basex = (Math.sqrt(r * r - Math.pow(h / 2, 2)) * (y3 - y0)) / h;
        var basey = (Math.sqrt(r * r - Math.pow(h / 2, 2)) * (x0 - x3)) / h;

        var startAngle = Math.atan2(y0 - y4 + basey, x0 - x4 + basex);
        this.arc(x4 - basex, y4 - basey, r, startAngle, startAngle + a);
      }
    };
    CanvasPolyLine.prototype.ellipse = function ellipse (x, y, rx, ry, rot, a0, a1, ccw) {
      var ref = applyToPoint(inverse(this._matrix), [
        this._x1,
        this._y1
      ]);
        var _x1 = ref[0];
        var _y1 = ref[1];

      if (a0 < 0) { a0 = (a0 + tau) % tau; }
      if (a1 < 0) { a1 = (a1 + tau) % tau; }

      var maxR = Math.max(rx, ry);

      var a = ccw ? a0 - a1 : a1 - a0;
      if (a < 0) { a = (a + tau) % tau; }
      var inc =
        1 / Math.sqrt(maxR * this._arcScale - Math.pow(this._arcScale, 2));
      var n = Math.ceil(a / inc);
      var cw = ccw ? -1 : 1;

      if (rx < 0) { throw new Error(("negative x radius: " + rx)); }
      if (ry < 0) { throw new Error(("negative y radius: " + ry)); }

      for (var c = 0; c <= n; c++) {
        var i = c === n ? a1 : a0 + c * inc * cw;

        var x0 =
          x -
          ry * Math.sin(i) * Math.sin(rot * Math.PI) +
          rx * Math.cos(i) * Math.cos(rot * Math.PI);
        var y0 =
          y +
          rx * Math.cos(i) * Math.sin(rot * Math.PI) +
          ry * Math.sin(i) * Math.cos(rot * Math.PI);

        // Is this path empty? Move to (x0,y0).
        if (!c) {
          if (this._x1 === null) {
            this.moveTo(x0, y0);
          }
          // Or, is (x0,y0) not coincident with the previous point? Line to (x0,y0).
          else if (Math.abs(_x1 - x0) > epsilon || Math.abs(_y1 - y0) > epsilon) {
            this.lineTo(x0, y0);
            continue;
          }
        }

        this.lineTo(x0, y0);
      }
    };
    CanvasPolyLine.prototype.rect = function rect (x, y, w, h) {
      this.moveTo(x, y);
      this.lineTo(x + w, y);
      this.lineTo(x + w, y + h);
      this.lineTo(x, y + h);
      this.lineTo(x, y);
    };
    CanvasPolyLine.prototype.toString = function toString$$1 () {
      if (!this._.length) { return ""; }
      return this._.map(function (d) { return d.join(" "); }).join("\n");
    };

    Object.defineProperties( CanvasPolyLine.prototype, prototypeAccessors );

    function arraysEqual(a, b) {
      if (a === b) { return true; }
      if (a == null || b == null) { return false; }
      if (a.length != b.length) { return false; }

      for (var i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) { return false; }
      }
      return true;
    }

    return CanvasPolyLine;

})));
//# sourceMappingURL=canvas-polyline.umd.js.map
